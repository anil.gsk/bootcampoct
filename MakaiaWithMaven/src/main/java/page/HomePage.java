package page;

import org.openqa.selenium.WebElement;

import lib.selenium.WebDriverServiceImpl;

public class HomePage extends WebDriverServiceImpl{
	
	public HomePage clickAppLauncher() {
		click(locateElement("class","slds-icon-waffle"));
		return this;
	}
	
	public HomePage clickViewAll() {
		click(locateElement("xpath","//button[text()='View All']"));
		return this;
	}
	
	
}
