package page;

import lib.selenium.WebDriverServiceImpl;

public class LogInPage extends WebDriverServiceImpl{

	public LogInPage typeUserName(String uName) {
//		WebElement eleUserName = locateElement("username");
		type(locateElement("username"), uName);
		return this;
	}
	
	public LogInPage typePassword(String pwd) {
		type(locateElement("password"), pwd);
		return this;
	}
	
	public HomePage clickLogin() {
		click(locateElement("Login"));
		return new HomePage();
	}
	
}
