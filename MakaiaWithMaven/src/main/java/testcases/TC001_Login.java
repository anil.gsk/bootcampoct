package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import lib.selenium.PreAndPost;
import page.LogInPage;

public class TC001_Login extends PreAndPost {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Login";
		testDescription = "Login into Application";
		nodes = "basic";
		category = "smoke";
		authors = "sarath";
//		dataSheetName = "filename";
	}
	
	@Test
	public void login() {
		new LogInPage()
		.typeUserName("bowyakarthikeyan@testleaf.com")
		.typePassword("India@123")
		.clickLogin()
		.clickAppLauncher()
		.clickViewAll();
	}	

}
